package mphonenbook.myphonebook;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    public static final int REQUEST_CODE_ADD = 1;
    public static final int REQUEST_CODE_UPDATE = 2;

    List<Contact> contacts = new ArrayList<>();

    ListView lvContacts;

    MyContactAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        lvContacts = (ListView) findViewById(R.id.lv_contacts);
        contacts = PhonebookDatabase.getInstance(this).phonebookDAO().getAllContacts();
        adapter = new MyContactAdapter(this, contacts);
        lvContacts.setAdapter(adapter);

        Button addBtn = (Button) findViewById(R.id.bt_add);
        addBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(new Intent(MainActivity.this, DataInputActivity.class), REQUEST_CODE_ADD);
            }
        });

        lvContacts.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(MainActivity.this);
                alertDialogBuilder.setTitle("Delete contact");
                alertDialogBuilder
                        .setMessage("Are you sure?")
                        .setCancelable(false)
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                PhonebookDatabase.getInstance(MainActivity.this).phonebookDAO().deleteContact(contacts.get(position).getId());
                                contacts.removeAll(contacts);
                                contacts.addAll(PhonebookDatabase.getInstance(MainActivity.this).phonebookDAO().getAllContacts());
                                adapter.notifyDataSetChanged();
                                dialog.dismiss();
                            }
                        })
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.dismiss();
                            }
                        });
                alertDialogBuilder.create().show();
            }
        });

        lvContacts.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener()
        {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                Intent uIntent = new Intent(MainActivity.this, DataInputActivity.class);
                uIntent.putExtra("contact", contacts.get(position));
                uIntent.putExtra("position", position);
                startActivityForResult(uIntent, REQUEST_CODE_UPDATE);
                return true;
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != RESULT_OK)
            return;

        if (requestCode == REQUEST_CODE_ADD) {
            contacts.add((Contact) data.getExtras().getSerializable("contact"));
            PhonebookDatabase.getInstance(MainActivity.this).phonebookDAO().addContact((Contact) data.getExtras().getSerializable("contact"));
            contacts.removeAll(contacts);
            contacts.addAll(PhonebookDatabase.getInstance(MainActivity.this).phonebookDAO().getAllContacts());
            adapter.notifyDataSetChanged();
        } else if (requestCode == REQUEST_CODE_UPDATE){
            PhonebookDatabase.getInstance(MainActivity.this).phonebookDAO().updateContact((Contact) data.getExtras().getSerializable("contact"));
            contacts.removeAll(contacts);
            contacts.addAll(PhonebookDatabase.getInstance(MainActivity.this).phonebookDAO().getAllContacts());
            adapter.notifyDataSetChanged();
        }
    }
}
