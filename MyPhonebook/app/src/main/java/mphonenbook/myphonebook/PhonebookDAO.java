package mphonenbook.myphonebook;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

/**
 * Created by ggero-i7 on 15/10/2017.
 */

@Dao
public interface PhonebookDAO {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void addContact(Contact contact);

    @Query("SELECT * FROM contact")
    List<Contact> getAllContacts();

    @Update(onConflict = OnConflictStrategy.REPLACE)
    void updateContact(Contact contact);

    @Query("DELETE FROM contact WHERE id = :id")
    void deleteContact(int id);

}
