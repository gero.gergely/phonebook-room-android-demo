package mphonenbook.myphonebook;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

/**
 * Created by ggero-i7 on 15/10/2017.
 */

@Database(entities = {Contact.class}, version = 1, exportSchema = false)
public abstract class PhonebookDatabase extends RoomDatabase {

    private static PhonebookDatabase instance;

    public abstract PhonebookDAO phonebookDAO();

    public static PhonebookDatabase getInstance(Context context){
        if (instance == null){
            instance = Room.databaseBuilder(context, PhonebookDatabase.class, "phonebookdatabase")
                    .allowMainThreadQueries()
                    .fallbackToDestructiveMigration()
                    .build();
        }

        return instance;
    }

    public static void destroyInstance() {
        instance = null;
    }
}
