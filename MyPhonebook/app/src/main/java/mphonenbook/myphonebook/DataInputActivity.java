package mphonenbook.myphonebook;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

/**
 * Created by ggero-i7 on 15/10/2017.
 */

public class DataInputActivity extends AppCompatActivity {

    protected EditText etName;
    protected EditText etPhone;
    protected Button btAdd;

    private int id2Update;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.data_input);

        etName = (EditText) findViewById(R.id.et_name);

        etPhone = (EditText) findViewById(R.id.et_phone);

        btAdd = (Button) findViewById(R.id.bt_add);

        if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("contact")) {
            Contact contact2Update;
            contact2Update = (Contact) getIntent().getExtras().getSerializable("contact");
            etName.setText(contact2Update.getName());
            etPhone.setText(contact2Update.getPhone());
            id2Update = contact2Update.getId();
        }

        btAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //TODO check data valability
                Intent result = new Intent();
                Contact contact = new Contact(etName.getText().toString(), etPhone.getText().toString());
                if(id2Update > 0)
                    contact.setId(id2Update);
                result.putExtra("contact", contact);
                if(getIntent().getExtras() != null && getIntent().getExtras().containsKey("position"))
                    result.putExtra("position", getIntent().getExtras().getInt("position"));
                setResult(RESULT_OK, result);
                finish();

            }
        });
    }
}
